@extends('mainLayout')

{{-- @section('title')
	BRAND
@endsection --}}

{{-- CAROUSEL --}}

@section('carousel')

	{{-- @include('layouts.partials.carousel') --}}
	<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
	  <ol class="carousel-indicators">
	    <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
	    <li data-target="#carouselIndicators" data-slide-to="1"></li>
	    <li data-target="#carouselIndicators" data-slide-to="2"></li>
	  </ol>
	<div class="carousel-img carousel-inner">
	    @foreach ($products{{-- ->take(3) --}} as $product)
	      <div class="carousel-item @if($loop->first) active @endif">
	      	<div class="row">
	      		<div class="col-lg-4 col-md-4 col-sm-4">
		        	<a href="#"><img class="d-block w-100" src="{{ asset('images/products/'.$product->image) }}" alt="{{ $product->name }}">
	      		</div></a>
	      		<div class="col-lg-4 col-md-4 col-sm-4">
		        	<a href="#"><img class="d-block w-100" src="{{ asset('images/products/'.$product->image) }}" alt="{{ $product->name }}">
	      		</div></a>
	      		<div class="col-lg-4 col-md-4 col-sm-4">
		        	<a href="#"><img class="d-block w-100" src="{{ asset('images/products/'.$product->image) }}" alt="{{ $product->name }}">
	      		</div></a>
	      	</div>
	      </div>
	    @endforeach
	  <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
  	</div>
</div>

	<script type="text/javascript">

		$('.carousel').carousel()
		
	</script>

@endsection
