<footer class="footer mt-auto py-5">
 	<div class="footer-container">
		<div class="footer-top">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 row top">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<h3>Be the first to know</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
					</div>
					<!-- Input Email -->
					<div class="col-lg-6 col-md-6 col-sm-6">
						<form class="form-inline">
							<div>
								<input class="form-control bg-dark" type="email" name="email" id="promo-mail" aria-describedby="emailHelp" value="" placeholder="Email adress" autocomplete="off">
							</div>
							<!-- Submit button -->
							<div class="input-group-append">
								<button class="btn btn-light" id="promo-mail" type="submit">submit</button>
							</div>
						</form>
						<!-- advisor msg -->
						<div>
							<small class="form-text text-muted promo-mail">Only for promos</small>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr align="center" noshade="noshade" size="2" width="90%" />
		<!-- Footer Middle -->
		<div class="footer-middle">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 row middle">
					<div class="col-lg-4 col-md-4 col-sm-4 col-lg-offset-2">
						<div class="info">
							<h4>Visit us</h4>
							<ul>
								<li>
									<p>
										<b>ADDRESS</b>
										<br>
										Lorem Ipsum 123
									</p>
								</li>
								<li>
									<p>
										<b>TELEPHONE</b>
										<br>
										01 23 45 67 89
									</p>
								</li>
								<li>
									<p>
										<b>EMAIL</b>
										<br>
										<a href="mailto:alancebohin92@gmail.com">alancebohin92@gmail.com</a>
									</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-lg-offset-2">
						<div class="info">
							<h4>Working days and hours</h4>
							<span>MON - FRI / 9:00 AM - 7:00 PM</span>
							<hr align="left" noshade="noshade" size="2" width="30%" />
							<span>SAT - SUN / 10:00 AM - 14:00 PM</span>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 row purchase">
							<img src="{{ asset('images/shipping.png') }}">&nbsp;|&nbsp;
							<img src="{{ asset('images/card.png') }}">&nbsp;|&nbsp;
							<img src="{{ asset('images/safe.png') }}">&nbsp;
					</div>
				</div>
			</div>
		</div>
		<hr align="center" noshade="noshade" size="2" width="90%" />
		<br>
		<div class="container footer-bottom">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
			    	<span class="text-muted gitlab">© https://gitlab.com/Alan_Cebohin/ecommerce-laravel. 2019</span>
				</div>
				<div class="mx-auto row social">
					<img class="fb-link" href="#" src="{{ asset('images/socialMedia/fb.png') }}" alt="facebook icon">&nbsp;
					<img class="ig-link" href="#" src="{{ asset('images/socialMedia/ig.png') }}" alt="instagram icon">&nbsp;
					<img class="in-link" href="#" src="{{ asset('images/socialMedia/in.png') }}" alt="linkedin icon">&nbsp;
				</div>
			</div>
		</div>

	</div>
</footer>