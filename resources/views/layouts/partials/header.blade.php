{{-- Main navigation header --}}
<div>
	  {{-- GUEST HEADER --}}
	  	@if (Auth::check() && (Auth::user()->is_admin != 1))
	  		@include('layouts.partials.guest_header')
		@endif
		{{-- ADMIN HEADER --}}
		@if (Auth::check() && (Auth::user()->is_admin == 1))
	  		@include('layouts.admin.admin_header')
	  	{{-- VISITOR HEADER --}}
		@elseif(! Auth::check())
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <a class="navbar-brand" href="/">Home</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse header" id="navbarNavAltMarkup">
		    <div class="navbar-nav">
		      <a class="nav-item nav-link {{-- active --}}" href="#">Wishlist</a>
		      <a class="nav-item nav-link" href="/register">Register</a>
		      <a class="nav-item nav-link" href="/login">Login</a>
		    </div>
		  </div>
	  @endif
	</nav>
</div>
{{-- Language brand and search section --}}
<div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-2 container brand">
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4">			
			{{-- Currency dropdown --}}
			<div class="btn-group currency">
		  		<button type="button" class="btn bg-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Currency</button>
				<div class="dropdown-menu">
				    <a class="dropdown-item selected" href="#">USD</a>
				    <a class="dropdown-item" href="#">EUR</a>
				    <a class="dropdown-item" href="#">KR</a>
				</div>
			</div>
			{{-- languages dropdown --}}
			<div class="btn-group languages">
		  		<button type="button" class="btn bg-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Lang.(Flag)</button>
				<div class="dropdown-menu">
				    <a class="dropdown-item selected" href="#">USA</a>
				    <a class="dropdown-item" href="#">DK</a>
				    <a class="dropdown-item" href="#">ES</a>
				</div>
			</div>
		</div>
		{{-- logo --}}
		<div class="col-lg-4 col-md-4 col-sm-4">
			<div class="logo">
				<img src="{{ asset('images/safe.png') }}" alt="logo">
			</div>
		</div>
		{{-- Search input --}}
		<div class="col-lg-4 col-md-4 col-sm-4 search">
			<form class="form-inline" action="#">
		    <input class="form-control" type="search" placeholder="Search" aria-label="Search">
		    <button class="btn btn-outline-dark" type="submit"><i class="fa fa-search"></i></button>
			</form>
		</div>
	</div>
</div>
<hr align="center" noshade="noshade" size="2" width="90%" />
{{-- Menú nav header --}}
<div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-2 container bg-white menu">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 menu-left">
			<div>
				<nav class="navbar navbar-expand-lg">
					<div class="btn-group collections">
						<button type="button" class="btn bg-white dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Collection</button>
					</div>
				</nav>
			</div>
			<div>
				<nav class="navbar navbar-expand-lg">
					<div class="btn-group products">
						<button type="button" class="btn bg-white dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Products</button>
						<div class="dropdown-menu">
{{-- 							@foreach ($categories as $category)
						    	<a class="dropdown-item selected" href="categories/{{ $category->name }}">{{ $category->name }}</a>
							@endforeach --}}
						</div>
					</div>
				</nav>
			</div>
			<div>
				<nav class="navbar navbar-expand-lg">
					<div class="btn-group pages">
						<button type="button" class="btn bg-white dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</button>
					</div>
				</nav>
			</div>
			<div>
				<nav class="navbar navbar-expand-lg">
					<div class="btn-group offers">
						<button type="button" class="btn bg-white dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Special offers!</button>
					</div>
				</nav>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 menu-right">
			<div id="cart">
				<div class="btn-group shopping-bag">
					<button type="button" class="btn bg-white dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('images/shopping-bag.png') }}"></button>
				</div>
			</div>
		</div>
	</div>
	<hr align="center" size="2" width="90%" />
</div>
