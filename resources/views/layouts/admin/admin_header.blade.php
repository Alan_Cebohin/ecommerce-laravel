<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="/">Home</a>
	<div class="collapse navbar-collapse header" id="navbarNavAltMarkup">
		<div class="btn-group logged d-flex justify-content-end">
	  		<div class="navbar-nav">
	  			<a class="nav-item nav-link {{-- active --}}" href="/admin">Dashboard</a>
	  		</div>
	  		<button type="button" class="btn bg-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</button>
			<div class="dropdown-menu">
			    <a class="dropdown-item selected" href="#">My Profile</a>
			    <a class="dropdown-item" href="/logout">Logout</a>
			</div>
		</div>
	</div>
</nav>