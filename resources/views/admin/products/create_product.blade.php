<!DOCTYPE html>
<html>
<head>
	<title>Create products</title>
	@include('layouts.partials.head')
</head>
<body>
	@include('layouts.admin.admin_header')

	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-md-8">
	            <div class="card">
	                <div class="card-header">{{ __('Create product') }}</div>

	                <div class="card-body">

		{!! Form::open(['action' => 'ProductsController@store', 'files' => true]) !!}
		
					 	<div class="form-group">
							{!! Form::label('name', 'Name') !!}
							{!! Form::text('name', null, array('class' => 'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('slug', 'Slug') !!}
							{!! Form::text('slug', null, array('class' => 'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('details', 'Details') !!}
							{!! Form::text('details', null, array('class' => 'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('price', 'Price') !!}
							{!! Form::text('price', null, array('class' => 'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('description', 'Description') !!}
							{!! Form::text('description', null, array('class' => 'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('category', 'Category') !!}
							{!! Form::select('category', $categories, null, array('class' => 'form-control', 'placeholder'=>'Select category')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('image', 'Image') !!}
							{!! Form::file('image', array('class' => 'form-control')) !!}
						</div>
							{!! Form::submit('Create', array('class' => 'btn btn-primary')) !!}
		{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
