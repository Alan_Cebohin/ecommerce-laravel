<!DOCTYPE html>
<html lang="en">
<head>
	<title>@yield('title', 'Alan Cebohin-ecommerce-Laravel')</title>

	@include('layouts.partials.head')

</head>
<body>


	@include('layouts.partials.header')

	{{-- Authentication --}}
	@yield('register')
	@yield('login')
	{{-- Already logged --}}
	@yield('logged')
	@yield('verify')
	{{-- Forgot password --}}
	@yield('password_reset')
	@yield('confirm_reset')
	
	{{--HOME: Carousel should be only visible in home view --}}
	@yield('carousel')

	{{-- PRODUCTS: General view for all products --}}

	@yield('products')

	@include('layouts.partials.footer')
	@include('layouts.partials.footerscripts')


</body>

	<script src="{{ asset('js/cart.js') }}"></script>

</html>