@extends('mainLayout')


@section('products')
	<div class="col-lg-12 col-md-12 col-sm-12 products text-center">
		@foreach ($products as $product)
			<div class="col-lg-3 col-md-3 col-sm-3 product" style="display: inline-block;">
				<a href="#"><div class="product_name">{{ $product->name }}</div></a>
				<a href="#"><div class="product_price">{{ $product->price }}</div></a>
				<a href="#"><div class="product_price"><img src="{{ asset('images/products/'.$product->image) }}" alt="Product image" width="150px" height="150px"></div></a>
			</div>
		@endforeach
	</div>
@endsection