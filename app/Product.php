<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table = 'products';
    protected $fillable = [
        'name', 'slug', 'details', 'price', 'description', 'category_id', 'image,'
    ];

    public function category()
    {
    	$this->belongsTo(Category::class);
    }
}
