<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// HOME
Route::get('/', 'HomeController@index');

// ADMIN
Route::group(['middleware' => ['auth', 'is_admin']], function()
{
	Route::get('/admin', 'AdminController@index');
	Route::get('/admin/products/create', 'ProductsController@create');
	Route::post('/admin/products/create', 'ProductsController@store');
});

// REGISTER, LOGIN, RESET PASSWORD
Route::get('/register', 'RegisterController@index');
Route::get('/login', 'LoginController@index');

// FORGOT PASSWORD
Route::get('/password/reset', 'ResetPasswordController@index');

// LOGOUT
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


// PRODUCTS
Route::get('/products', 'ProductsController@index');
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
