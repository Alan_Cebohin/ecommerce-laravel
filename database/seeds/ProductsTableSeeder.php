<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
        	'name' => 'Minimalist Shirt',
        	'slug' => 'minimalist_shirt',
        	'details' => 'cotton',
        	'price' => 100,
        	'description' => 'lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        	'category_id' => 1,
            'image' => '',
        ]);

        Product::create([
        	'name' => 'Minimalist Pants',
        	'slug' => 'minimalist_pants',
        	'details' => 'jean',
        	'price' => 150,
        	'description' => 'lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        	'category_id' => 2,
            'image' => '',
        ]);

        Product::create([
        	'name' => 'Van der rohe',
        	'slug' => 'van_der_rohe',
        	'details' => 'leather',
        	'price' => 200,
        	'description' => 'lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        	'category_id' => 3,
            'image' => '',
        ]);
        Product::create([
        	'name' => 'Minimalist Shirt_1',
        	'slug' => 'minimalist_shirt_1',
        	'details' => 'cotton',
        	'price' => 100,
        	'description' => 'lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        	'category_id' => 1,
            'image' => '',
        ]);

        Product::create([
        	'name' => 'Minimalist Pants_1',
        	'slug' => 'minimalist_pants_1',
        	'details' => 'jean',
        	'price' => 150,
        	'description' => 'lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        	'category_id' => 2,
            'image' => '',
        ]);

        Product::create([
        	'name' => 'Van der rohe_1',
        	'slug' => 'van_der_rohe_1',
        	'details' => 'leather',
        	'price' => 200,
        	'description' => 'lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        	'category_id' => 3,
            'image' => '',
        ]);
        Product::create([
        	'name' => 'Minimalist Shirt_2',
        	'slug' => 'minimalist_shirt_2',
        	'details' => 'cotton',
        	'price' => 100,
        	'description' => 'lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        	'category_id' => 1,
            'image' => '',
        ]);

        Product::create([
        	'name' => 'Minimalist Pants_2',
        	'slug' => 'minimalist_pants_2',
        	'details' => 'jean',
        	'price' => 150,
        	'description' => 'lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        	'category_id' => 2,
            'image' => '',
        ]);

        Product::create([
        	'name' => 'Van der rohe_2',
        	'slug' => 'van_der_rohe_2',
        	'details' => 'leather',
        	'price' => 200,
        	'description' => 'lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        	'category_id' => 3,
            'image' => '',
        ]);
    }
}
