<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
        	'name' => 'shirts',
        ]);

        Category::create([
        	'name' => 'pants',
        ]);

        Category::create([
        	'name' => 'shoes',
        ]);
    }
}
